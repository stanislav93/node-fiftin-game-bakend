'use strict';
const express = require('express');
const https = require('https');
const app = express();
const redirectUrl = 'https://node-fiftin-game1.appspot.com/';
const frontend = `https://fifteen-game1.firebaseapp.com/`;

const request = require("request-promise-node");
var session = require('express-session');

app.use((req, res, next) => {
    res.set({
        'Access-Control-Allow-Origin': 'https://fifteen-game1.firebaseapp.com',
        'Access-Control-Allow-Credentials': true,
        'Access-Control-Max-Age': '1000',
        'Access-Control-Allow-Headers': 'content-type',
        'Access-Control-Allow-Method': 'GET,PUT,POST,DELETE',
    });
    next();
});
app.use(session({secret: 'keyboard cat', cookie: {maxAge: 84400}}));
app.get('/redirect', async (req, res) => {
    res.redirect(`https://oauth.vk.com/authorize?client_id=6758111%20&display=page&redirect_uri=${redirectUrl}&scope=friends`);
});
app.get('/', async (req, res) => {
    if (req.query.code === undefined || req.query.code === '') {
        res.redirect(frontend);
    } else {
        const result = await request(`https://oauth.vk.com/access_token?client_id=6758111&client_secret=y2V4SKcxNwP9HAWU8zl6&redirect_uri=${redirectUrl}&code=${req.query.code}`);
        const parsDate = JSON.parse(result.body);
        res.redirect(`${frontend}?token=${parsDate.access_token}`);
    }
});
app.get('/fiend', async (req, res) => {
    const
        token = req.query.token,
        result = await request(`https://api.vk.com/method/friends.get?access_token=${token}&fields=first_name&count=5&order=name&version=5.8`);
    var parsDate;
    try {
        parsDate = JSON.parse(result.body).response;
    } catch (error) {
        parsDate = [];
    }
    res.send(JSON.stringify(parsDate)).end();
});
app.get('/user', async (req, res) => {
    const
        token = req.query.token,
        result = await request(`https://api.vk.com/method/users.get?access_token=${token}&fields=first_name&version=5.8`);
    var parsDate;
    try {
        parsDate = JSON.parse(result.body).response[0];
    } catch (error) {
        parsDate = [];
    }
    res.send(JSON.stringify(parsDate)).end();
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}`);
    console.log('Press Ctrl+C to quit.');
});
